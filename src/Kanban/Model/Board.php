<?php

namespace Kanban\Model;

class Board extends Model
{
    protected $id;
    protected $name;
    protected $name_with_namespace;

    public function __construct($fields)
    {
        foreach ($fields as $field => $value) {
            $this->{$field} = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNameWithNamespace()
    {
        return $this->name_with_namespace;
    }

    /**
     * @param mixed $name_with_namespace
     */
    public function setNameWithNamespace($name_with_namespace)
    {
        $this->name_with_namespace = $name_with_namespace;
    }

} 