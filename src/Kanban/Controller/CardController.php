<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CardController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Get list cards by projects
     *
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $projectId = $request->query->get('project_id', 0);
        $state = $request->query->get('state', 'opened');
        $per_page = $request->query->get('per_page', 100);

        $response  = $this->app['gitlab_api']->executeCommand('GetIssuesByProject',
            ['project_id' => $projectId,
             'state' => $state,
             'per_page' =>$per_page]
        );

        return $response;
    }

    /**
     * Get one card for project
     *
     * @param Request $request
     *
     * @return array
     */
    public function cardAction(Request $request)
    {
        $project_id = $request->query->get('project_id', 0);
        $issue_id   = $request->query->get('issue_id', 0);

        $response   = $this->app['gitlab_api']->executeCommand('GetIssue', [
            'project_id' => $project_id,
            'issue_id'   => $issue_id
        ]);

        return $response;
    }

    /**
     * Create new card
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $response   = $this->app['gitlab_api']->executeCommand('CreateIssue', $request->request->all());

        return $response;
    }

    /**
     * @param Request $request
     * @return array
     *
     */
    public function updateAction(Request $request)
    {
        $content = $request->request->all();

        $content['description'] = trim($content['description'], PHP_EOL);
        $content['description'] .= PHP_EOL . $this->encodeTodo($content['todo']);
        $content['description'] .= PHP_EOL . $this->encodeProperties($content['properties']);

        $response = $this->app['gitlab_api']->executeCommand('EditIssue', $content);

        return $response;
    }

    /**
     * move card between stages
     *
     * @param Request $request
     * @return mixed
     */
    public function moveToAction(Request $request)
    {
        $content = $request->request->all();
        $pattern = "/KB\[stage\]\[\d\]\[(.*)\]/";
        preg_match($pattern, $content['stage']['source'], $current);
        preg_match($pattern, $content['stage']['dest'], $new);

        $this->app['gitlab_api']->executeCommand('CreateComment',
            [
                'project_id' => $content['project_id'],
                'issue_id' => $content['issue_id'],
                'body' => "moved issue from **".$current[1]."** to **".$new[1]."**",
            ]
        );

        $content['description'] = trim($content['description'], PHP_EOL);
        $content['description'] .= PHP_EOL . $this->encodeTodo($content['todo']);
        $content['description'] .= PHP_EOL . $this->encodeProperties($content['properties']);

        $response = $this->app['gitlab_api']->executeCommand('EditIssue', $content);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function deleteAction(Request $request)
    {
        $vars = $request->request->all();

        $vars['state_event'] = 'close';
        $response = $this->app['gitlab_api']->executeCommand('DeleteIssue', $vars);

        return $response;
    }

    /**
     * Converter todo from array to string for description
     *
     * @param array $todoList todo list
     *
     * @return string
     */
    protected function encodeTodo($todoList)
    {
        $result = '';

        foreach ($todoList as $todo) {
            $result .= '- [' . ($todo['checked'] ? 'x' : ' ') . '] ' . $todo['body'] . PHP_EOL;
        }

        return $result;
    }

    /**
     * Converter issue properties to html comment
     *
     * @param string $properties card properties
     *
     * @return string
     */
    protected function encodeProperties($properties)
    {
        return "<!-- @KB:".json_encode($properties)." -->";
    }
}
