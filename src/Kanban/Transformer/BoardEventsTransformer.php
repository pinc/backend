<?php

namespace Kanban\Transformer;

use Gitlab\Models\ProjectEvent;
use League\Fractal\TransformerAbstract;

class BoardEventsTransformer extends TransformerAbstract
{
    public function transform(ProjectEvent $event)
    {
        $result = [
            'project_id'  => $event->getProjectId(),
            'title'       => $event->getTitle(),
            'action_name' => $event->getActionName(),
            'target_id'   => $event->getTargetId(),
            'target_type' => $event->getTargetType(),
            'author_id'   => $event->getAuthorId(),
        ];

        return $result;
    }
}
