<?php

namespace Kanban\Transformer;

use Gitlab\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    protected $infoPatterns = [
        'Reassigned to .*?',
        'Milestone changed to .*?',
        'Title changed from .*? to .*?',
        'Added .*? label(s)?',
        'mentioned in commit .*?',
        'mentioned in merge request .*?',
        'Status changed to closed',
        'Status changed to reopened',
        'moved issue from .*? to .*?',
        'Marked as \*\*blocked\*\*: .*?',
        'Marked as \*\*ready\*\* for next stage',
        'Marked as \*\*unblocked\*\*',
        'mentioned in issue .*?',
    ];

    public function transform(Comment $comment)
    {
        $result = [
            'id'         => $comment->getId(),
            'author'     => $comment->getAuthor(),
            'body'       => $comment->getBody(),
            'is_info'    => $this->getCommentType($comment->getBody()),
            'created_at' => $comment->getCreatedAt()->getTimestamp(),
        ];

        return $result;
    }

    /**
     * @param $body
     *
     * @return string
     */
    protected function getCommentType($body)
    {
        return 1 == preg_match('/^'.implode('|', $this->infoPatterns).'$/', $body);
    }
}

